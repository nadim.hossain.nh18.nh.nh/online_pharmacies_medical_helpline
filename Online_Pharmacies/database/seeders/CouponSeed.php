<?php

namespace Database\Seeders;

use App\Models\Coupon;
use Illuminate\Database\Seeder;

class CouponSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Coupon::create([
            'code'=>'ABC123',
            'date'=>'2022-6-30',
            'value'=>30
        ]);

        Coupon::create([
            'code'=>'DEF456',
            'date'=>'2022-7-20',
            'value'=>30
        ]);
    }
}
