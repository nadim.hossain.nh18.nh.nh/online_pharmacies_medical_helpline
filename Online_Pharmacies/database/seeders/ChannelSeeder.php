<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class ChannelSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('channels')->insert([
            'channelname' => 'Icenter',
            'channeltype' => 'Phone',
            'bankaccountnumber' => '75812581825',
            'sellerid' => '1'
        ]);

        DB::table('channels')->insert([
            'channelname' => 'FitnessBlender',
            'channeltype' => 'Health & Fitness',
            'bankaccountnumber' => '96751235472',
            'sellerid' => '2'
        ]);

        DB::table('channels')->insert([
            'channelname' => 'Love Sweat Fitness',
            'channeltype' => 'Health & Fitness',
            'bankaccountnumber' => '74536985120',
            'sellerid' => '3'
        ]);
    }
}