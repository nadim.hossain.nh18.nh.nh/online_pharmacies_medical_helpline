<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Carbon\Carbon;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $mytime = Carbon::today();

        DB::table('products')->insert([
            'productname' => 'Abacavir',
            'price' => '120000',
            'quantity' => '100',
            'tprice' => '120000000',
            'date' => $mytime,
            'image' => '',
            'channelid' => '1'
        ]);

        DB::table('products')->insert([
            'productname' => 'Adrenaclick epinephrine',
            'price' => '40000',
            'quantity' => '120',
            'tprice' => '4800000',
            'date' => $mytime,
            'image' => '',
            'channelid' => '1'
        ]);

        DB::table('products')->insert([
            'productname' => 'Advil (ibuprofen)',
            'price' => '20000',
            'quantity' => '50',
            'tprice' => '1000000',
            'date' => $mytime,
            'image' => '',
            'channelid' => '1'
        ]);

        DB::table('products')->insert([
            'productname' => 'efinaconazole Jublia',
            'price' => '100000',
            'quantity' => '150',
            'tprice' => '15000000',
            'date' => $mytime,
            'image' => '',
            'channelid' => '1'
        ]);

        DB::table('products')->insert([
            'productname' => 'Elase (fibrinolysin w/dnase-topical ointment)',
            'price' => '90000',
            'quantity' => '40',
            'tprice' => '3600000',
            'date' => $mytime,
            'image' => '',
            'channelid' => '1'
        ]);

        DB::table('products')->insert([
            'productname' => 'elbasvir_and_grazoprevir',
            'price' => '900',
            'quantity' => '100',
            'tprice' => '90000',
            'date' => $mytime,
            'image' => '',
            'channelid' => '2'
        ]);

        DB::table('products')->insert([
            'productname' => 'Eminase (anistreplase-injection)',
            'price' => '500',
            'quantity' => '600',
            'tprice' => '300000',
            'date' => $mytime,
            'image' => '',
            'channelid' => '2'
        ]);

        DB::table('products')->insert([
            'productname' => 'Emtriva emtricitabine',
            'price' => '100',
            'quantity' => '600',
            'tprice' => '60000',
            'date' => $mytime,
            'image' => '',
            'channelid' => '2'
        ]);

        DB::table('products')->insert([
            'productname' => 'Erleada apalutamide',
            'price' => '950',
            'quantity' => '90',
            'tprice' => '85500',
            'date' => $mytime,
            'image' => '',
            'channelid' => '2'
        ]);

        DB::table('products')->insert([
            'productname' => 'Etravirine Intelence',
            'price' => '700',
            'quantity' => '500',
            'tprice' => '350000',
            'date' => $mytime,
            'image' => '',
            'channelid' => '2'
        ]);

        DB::table('products')->insert([
            'productname' => 'Kaopectate Advanced Formula (attapulgite)',
            'price' => '400',
            'quantity' => '500',
            'tprice' => '200000',
            'date' => $mytime,
            'image' => '',
            'channelid' => '3'
        ]);

        DB::table('products')->insert([
            'productname' => 'Ketoconazole vs Selenium Sulfide',
            'price' => '150',
            'quantity' => '600',
            'tprice' => '90000',
            'date' => $mytime,
            'image' => '',
            'channelid' => '3'
        ]);

        DB::table('products')->insert([
            'productname' => 'Klisyri tirbanibulin',
            'price' => '300',
            'quantity' => '100',
            'tprice' => '30000',
            'date' => $mytime,
            'image' => '',
            'channelid' => '3'
        ]);

        DB::table('products')->insert([
            'productname' => 'Kytril (granisetron-injection)',
            'price' => '600',
            'quantity' => '900',
            'tprice' => '540000',
            'date' => $mytime,
            'image' => '',
            'channelid' => '3'
        ]);

        DB::table('products')->insert([
            'productname' => 'Uni-Ease (docusate-oral)',
            'price' => '1000',
            'quantity' => '500',
            'tprice' => '500000',
            'date' => $mytime,
            'image' => '',
            'channelid' => '3'
        ]);
    }
}