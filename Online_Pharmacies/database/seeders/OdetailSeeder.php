<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class OdetailSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('odetails')->insert([
            'description' => 'Valo Phone',
            'image' => '',
            'productid' => '1',
            'orderid' => '1'

        ]);
    }
}
