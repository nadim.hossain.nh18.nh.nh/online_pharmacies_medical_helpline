<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class PaymentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('payments')->insert([
            'channelname' => 'Icenter',
            'amount' => '4000',
            'bankaccountnumber' => '75812581825',
            'adminid' => '1',
            'channelid' => '1'

        ]);

        DB::table('payments')->insert([
            'channelname' => 'FitnessBlender',
            'amount' => '5000',
            'bankaccountnumber' => '96751235472',
            'adminid' => '1',
            'channelid' => '2'

        ]);

        DB::table('payments')->insert([
            'channelname' => 'Love Sweat Fitness',
            'amount' => '1000',
            'bankaccountnumber' => '74536985120',
            'adminid' => '1',
            'channelid' => '3'

        ]);
    }
}
