<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Admin;
use App\Models\Buyer;
use App\Models\Deliveri;
use App\Models\Seller;
use App\Models\User;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Auth;

class AdminLoginController extends Controller
{
    public function login(){
        return view('admin.login');
    }

    public function loginsubmit(Request $request){
        $validate = Validator::make($request->all(), [
            "username"=>"required|min:5|max:10",
            "password"=>'required|min:6|regex:/^.*(?=.{3,})(?=.*[a-zA-Z])(?=.*[0-9])(?=.*[\d\x])(?=.*[!$#%]).*$/',
            ]);

        if($validate->fails()){
            return response()->json([
                'errors'=>$validate->getMessageBag(),
            ]);
        }
        else
        // $admin = Admin::where('username', $request->username)
        //     ->where('password' ,$request->password)
        //     ->first();

            // $user = Buyer::where('username', $request->username)
            // ->where('password' ,($request->password))
            // ->first();

            $admin_laravel = Admin::where('username', $request->username)
            ->where('password', $request->password)
            ->first();

            // $seller = Seller::where('username', $request->username)
            // ->where('password' ,($request->password))
            // ->first();

            // $deliveris = Deliveri::where('username', $request->username)
            // ->where('password' ,($request->password))
            // ->first();

            // if($admin){
            //     session()->put('admin',$request->uname);
            //     $token = $admin->createToken($admin->username.'_Token')->plainTextToken;

            //     return response()->json([
            //         'status' =>200,
            //         'username'=>$admin->username,
            //         'token'=>$token,
            //         'message'=>'Login Successfull',
            //     ]);
            // }
            // elseif($user){
            //     session()->put('admin',$request->username);
            //     if ($request->remember) {
            //         setcookie('remember',$request->username, time()+36000);
            //         Cookie::queue('name',$admin->username,time()+60);
            //     }
            //     return redirect()->route('customers');
            // }
            if($admin_laravel){
                session()->put('admin',$request->username);
                if ($request->remember) {
                    setcookie('remember',$request->username, time()+36000);
                    Cookie::queue('name',$admin_laravel->username,time()+60);
                }
                return redirect()->route('dashboard');
            }
            else{
                return response()->json([
                    'status' =>401,
                    'message'=> 'Login Failed. Try Again',
                ]);
            }

            // elseif($user){
            //     session()->put('user',$request->username);
            //     if ($request->remember) {
            //         setcookie('remember',$request->username, time()+36000);
            //         Cookie::queue('name',$admin->username,time()+60);
            //     }
            //     return redirect()->route('payment');
            // }

            // elseif($seller){
            //     session()->put('seller',$request->username);
            //     if ($request->remember) {
            //         setcookie('remember',$request->username, time()+36000);
            //         Cookie::queue('name',$admin->username,time()+60);
            //     }
            //     return redirect()->route('change_password');
            // }

            // elseif($deliveris){
            //     session()->put('deliveris',$request->username);
            //     if ($request->remember) {
            //         setcookie('remember',$request->username, time()+36000);
            //         Cookie::queue('name',$admin->username,time()+60);
            //     }
            //     return redirect()->route('accounts');
            // }

            // else{
            //     $error = "Username and Password Incorect. Try Again";
            //     return redirect()->route('login')
            //     ->with('error', $error);
            // }
    }

    public function logout(){
        Auth::user()->tokens->each(function($token, $key) {
            $token->delete();
        });
        return response()->json([
            'status' => 200,
            'message' => "Logged Out Successfull",
        ]);
    }
}
