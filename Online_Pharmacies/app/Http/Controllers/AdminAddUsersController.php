<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Buyer;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AdminAddUsersController extends Controller
{

    // public function __construct(){
    //     $this->middleware('ValidAdmin');
    // }

    public function addusers(){
        return view('admin.AdminAddUsers');
    }

    public function addusersubmit(Request $request){

        $validate = Validator::make($request->all(), [
            "uname"=>"required|min:5|max:10",
            "fname"=>"required|min:5|max:20",
            "address"=>"required|min:5|max:20",
            "password"=>'required|min:6|regex:/^.*(?=.{3,})(?=.*[a-zA-Z])(?=.*[0-9])(?=.*[\d\x])(?=.*[!$#%]).*$/',
            "cpassword"=>'required|min:6|regex:/^.*(?=.{3,})(?=.*[a-zA-Z])(?=.*[0-9])(?=.*[\d\x])(?=.*[!$#%]).*$/',
            "gender"=>'required',
            'phone'=>'required|regex:/^([0-9\s\-\+\(\)]*)$/',
            ]);

         if($validate->fails()){

            return response()->json([
                'errors'=>$validate->getMessageBag(),
            ]);
        }
        else{
            $user = new Buyer;

            $user->username = $request->input("uname");
            $user->password = $request->input("password");
            $user->name = $request->input("fname");
            $user->address= $request->input("address");
            $user->gender= $request->input("gender");
            $user->mobile= $request->input("phone");

            $user->save();

            $error = "Registraion Successfull";
            return Redirect()->route('addusers')
            ->with('success', $error);

            // $error = "Registraion Successfull";
            // return Redirect()->route('addusers')
            // ->with('success', $error);
        }
    }
}
