<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Models\Buyer;
use App\Models\Seller;
use Illuminate\Support\Js;
use Illuminate\Support\Facades\Validator;

class CustomersController extends Controller
{

    // public function __construct(){
    //     $this->middleware('ValidAdmin');
    // }

    function index()
    {
    	$data = DB::table('buyers')->get();
		$seller = DB::table('sellers')->get();
        $seller = Seller::paginate(5);

		// return response()->json([
		// 	'status' => 200,
		// 	'customers' => $data,
		// ]);
    	return view('admin.AdminCustomers', compact('data', 'seller'));
    }

	public function edit($id)
    {
        $student = Buyer::find($id);
        if($student)
        {
            // return response()->json([
            //     'status'=> 200,
            //     'student' => $student,
            // ]);
            return view('admin.AdminEditProfile', compact('student'));
        }
        else
        {
            return response()->json([
                'status'=> 404,
                'message' => 'No Users ID Found',
            ]);
        }

    }

	public function update(Request $request, $id)
    {
        $validate = Validator::make($request->all(), [
            "name"=>"required|min:5|max:20",
            "address"=>"required|min:5|max:20",
            "gender"=>'required',
            'mobile'=>'required|regex:/^([0-9\s\-\+\(\)]*)$/',
            ]);

			if($validate->fails()){

				return response()->json([
					'validationErrors'=>$validate->getMessageBag(),
				]);
			}
        else
        {
            $student = Buyer::find($id);
            if($student)
            {

                $student->name = $request->input('name');
                $student->mobile = $request->input('mobile');
				$student->gender = $request->input('gender');
				$student->address = $request->input('address');
                $student->update();

                // return response()->json([
                //     'status'=> 200,
                //     'message'=>'User Updated Successfully',
                // ]);
                $error = "Registraion Successfull";
                return Redirect()->route('profile')
                ->with('success', $error);
            }
            else
            {
                return response()->json([
                    'status'=> 404,
                    'message' => 'No User ID Found',
                ]);
            }
        }
    }

    public function destroy($id)
    {
        $student = Buyer::find($id);
        if($student)
        {
            $student->delete();
            // return response()->json([
            //     'status'=> 200,
            //     'message'=>'User Deleted Successfully',
            // ]);
            $error = "delete Successfull";
            return Redirect()->route('profile')
            ->with('success', $error);
        }
        else
        {
            return response()->json([
                'status'=> 404,
                'message' => 'No User ID Found',
            ]);
        }
    }
}

    function action(Request $request)
    {
    	if($request->ajax())
    	{
    		if($request->action == 'edit')
    		{
    			$data = array(
    				'buyerid'	    =>	$request->buyerid,
    				'username'		=>	$request->username,
					'name'	   		=>	$request->name,
    				'address'		=>	$request->address,
					'gender'	    =>	$request->gender,
    				'status'		=>	$request->status,
					'number'		=>	$request->number,
					'image'	   		=>	$request->image
    			);
    			DB::table('buyers')
    				->where('buyerid', $request->buyerid)
    				->update($data);
    		}
    		if($request->action == 'delete')
    		{
    			DB::table('buyers')
    				->where('buyerid', $request->buyerid)
    				->delete();
    		}
    		return response()->json($request);
    	}
    }

	function action2(Request $request)
    {
    	if($request->ajax())
    	{
    		if($request->action == 'edit')
    		{
    			$seller = array(
    				'sellerid'	    =>	$request->sellerid,
    				'username'		=>	$request->username,
					'name'	   		=>	$request->name,
    				'address'		=>	$request->address,
					'status'	    =>	$request->status,
    				'image'		    =>	$request->image,
					'number'		=>	$request->number
    			);
    			DB::table('sellers')
    				->where('sellerid', $request->sellerid)
    				->update($seller);
    		}
    		if($request->action == 'delete')
    		{
    			DB::table('sellers')
    				->where('sellerid', $request->sellerid)
    				->delete();
    		}
    		return response()->json($request);
    	}
    }
