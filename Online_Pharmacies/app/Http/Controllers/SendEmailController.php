<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Mail\VerifyMail;
use App\Models\Admin;
use App\Models\Code;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class SendEmailController extends Controller
{
    public function index(Request $request)
    {
     
     $validate = Validator::make($request->all(),[
          "email"=>'email:rfc,dns'
          ]);

       $check = Admin::where('email', $request->email)->first();

       if($validate->fails()){

        return response()->json([
            'errors'=>$validate->getMessageBag(),
        ]);

    }

        else{

            if($check){

                // Session::put('email', $request->emai);

                session()->put('email', $request->email);

                // $this->email = Auth::user()->$request->email;
 
                $myEmail = $request->email;
 
                $code = rand(0, 5000);

                // $this->code = Auth::user()->$code;

                session()->put('code', $code);
 
                $details = [
                'title' => 'Forget Password Code',
                'code' => $code
                ];
 
                Mail::to($myEmail)->send(new VerifyMail($details));

                $verity = new Code();

                $verity->code = $code;
                $verity->email = $request->input("email");

                $verity->save();
 
                return response()->json([
                    'status' =>200,
                    'code'=> $code,
                    'email'=>$request->email,
                    'message'=>'Check Your Mail Box.',
                ]);
           }
           else{
            return response()->json([
                'status' =>401,
                'message'=>'Something is wrong try again.',
            ]);
           }
        }
    }
}
