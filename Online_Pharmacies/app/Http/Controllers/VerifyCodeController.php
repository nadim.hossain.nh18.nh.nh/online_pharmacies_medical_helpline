<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Admin;
use App\Models\Code;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Session;

class VerifyCodeController extends Controller
{

    // public function __construct(){
    //     $this->middleware('ValidCode');
    // }

    public function forgetpassword(){
        return view('admin.AdminForgetPassword');
    }

    public function code(Request $request){
        $validate = Validator::make($request->all(),[
            "code"=>'required'
            ],
        );

        // $verify = Code::where('code', $request->code)->first();

        if($validate->fails()){

            return response()->json([
                'errors'=>$validate->getMessageBag(),
            ]);
        }
        else{

            $varify_code = Code::where('code', $request->code)->first();

            if($varify_code){

                return response()->json([
                    'status' =>200,
                    'message'=>'Verify Successfull',
                ]);
            }
            else{
                return response()->json([
                    'status' =>401,
                    'message'=>'Invalide Code',
                ]);
            }
        }
    }
    

    public function change(Request $request){
        $validate = Validator::make($request->all(),[
            "password"=>'required|min:6|regex:/^.*(?=.{3,})(?=.*[a-zA-Z])(?=.*[0-9])(?=.*[\d\x])(?=.*[!$#%]).*$/',
            "confirmpassword"=>'required|min:6|same:password|regex:/^.*(?=.{3,})(?=.*[a-zA-Z])(?=.*[0-9])(?=.*[\d\x])(?=.*[!$#%]).*$/'
            ],
            
        );

        if($validate->fails()){

            return response()->json([
                'errors'=>$validate->getMessageBag(),
            ]);
        }

        else{



            $check = Admin::where('email', "nadim4995@gmail.com")->first();

                $check->password = $request->confirmpassword;
                $check->save();

                return response()->json([
                    'status' =>200,
                    'message'=>'Password Change Successfull',
                ]);
        }
    }

}
