<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Admin;
use Illuminate\Http\Request;

class AdminEditProfilrController extends Controller
{

    // public function __construct(){
    //     $this->middleware('ValidAdmin');
    // }

    public function editprofile(Request $request){
        
        $edits = Admin::where('id', $request->id)->first();

        if($edits){
            return response()->json([
                'status'=> 200,
                'admin' => $edits,
            ]);

        }
    }

    public function adminEditSubmitted(Request $request){

        $admins = Admin::where('id', $request->id)->first();

        // foreach($admins as $admin){

            $admins->name = $request->fullname;
            $admins->email = $request->email;
            $admins->number = $request->number;
            $admins->gender = $request->gender;
            $admins->save();

        // }

        if($admins){
            return response()->json([
                'status'=> 200,
                'admin' => $admins,
            ]);
        }
    }




}
