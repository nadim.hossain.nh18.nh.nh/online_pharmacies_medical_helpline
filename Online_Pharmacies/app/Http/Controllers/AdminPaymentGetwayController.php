<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Order;
use Carbon\Carbon;

class AdminPaymentGetwayController extends Controller
{
    public function __construct(){
        $this->middleware('ValidAdmin');
    }

    public function payments(){

        $earm = Order::where('status', 'Done')->sum('tprice');

        $tex = $earm * (15)/100;
        $liabilities = $earm - $tex;
        $equaty = $earm - $liabilities;

        $mytime = Carbon::today();

        $tmoney = Order::where('status', 'Done')
        ->where('date', $mytime)
        ->sum('tprice');

        $tsale = Order::where('status', 'Done')
        ->sum('quantity');

        $todaysale = Order::where('status', 'Done')
        ->where('date', $mytime)
        ->sum('quantity');

        $orders = Order::all();
        $orders = Order::paginate(5);

        $moneys = Order::where('status', 'Done')->get();
        $moneys = Order::paginate(5);

        return view('admin.AdminPaymentsGetaway', compact('equaty', 'tmoney', 'tsale', 'todaysale', 'orders', 'moneys'));
    }
}
