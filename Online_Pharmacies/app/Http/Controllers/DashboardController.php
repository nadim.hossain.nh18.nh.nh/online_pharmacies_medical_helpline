<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Buyer;
use App\Models\Deliveri;
use Illuminate\Http\Request;
use App\Models\Seller;
use App\Models\Channel;
use App\Models\Order;
use App\Models\Product;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class DashboardController extends Controller
{
    public function __construct(){
        $this->middleware('ValidAdmin');
    }

    public function dashboard(){
        $buyer = Buyer::count();
        $seller = Seller::count();
        $activeseller = Seller::where('status', 'active')->count();
        $activebuyer = Buyer::where('status', 'active')->count();
        $delivery = Deliveri::where('status', 'active')->count();
        
        $earm = Order::where('status', 'Done')->sum('tprice');

        $tex = $earm * (15)/100;
        $liabilities = $earm - $tex;
        $equaty = $earm - $liabilities;



        $data = DB::table('buyers')->get();
        $data = Buyer::paginate(5);

        $cname = Channel::join('sellers', 'sellers.sellerid', '=', 'channels.sellerid')
                            ->get(['sellers.sellerid', 'sellers.username', 'channels.channelname']);

        $pname = Seller::join('channels', 'channels.sellerid', '=', 'sellers.sellerid')
                            ->join('products', 'products.channelid', '=', 'channels.channelid')
                            ->select(DB::raw("SUM(quantity) as product_quantity,channels.channelname, channels.channelid"))
                            ->orderBy("products.channelid")->groupBy(DB::raw("products.channelid", "products.channelname"))->get();
        
        $userData = Buyer::select(DB::raw("COUNT(*) as count"))
                            ->whereYear('created_at', date('Y'))
                            ->groupBy(DB::raw("Month(created_at)"))
                            ->pluck('count');



        // $users = Buyer::select(DB::raw("COUNT(*) as count"))
        //         ->whereYear('created_at', date('Y'))
        //         ->groupBy(DB::raw("Month(created_at)"))
        //         ->pluck('count');

        // $months = Buyer::select(DB::raw("Month(created_at) as month"))
        //         ->whereYear('created_at', date('Y'))
        //         ->groupBy(DB::raw("Month(created_at)"))
        //         ->pluck('month');

        // $tdatas = array(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);

        // foreach ($months as $key => $value) {
        //     $tdatas[$value] = $users[$key];
        // }

        // //  dd($tdatas);
        // // dd($months);
        // // dd($users);

        return view('admin.AdminDashboard', compact('buyer', 'seller', 'activeseller', 'activebuyer', 'delivery', 'data', 'cname', 'pname', 'equaty', 'userData'));
    }
}
