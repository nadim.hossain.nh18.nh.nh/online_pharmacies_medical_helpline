<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Admin;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AdminProfileController extends Controller
{
    // public function __construct(){
    //     $this->middleware('ValidAdmin');
    // }

    public function profile(){

        // $profile = Admin::all();


        // foreach($profile as $profiles){

        //     $uname = $profiles->username;
        //     $name = $profiles->name;
        //     $email = $profiles->email;
        //     $number = $profiles->number;
        //     $gender = $profiles->gender;
        //     $id = $profiles->id;
        // }

        $data = DB::table('admins')->get();

        if($data){

            return response()->json([
                'status' => 200,
                'admin' => $data,
            ]);
        }
    }
}
