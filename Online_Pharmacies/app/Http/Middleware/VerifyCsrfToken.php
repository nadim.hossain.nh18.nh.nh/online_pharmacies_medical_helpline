<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as Middleware;

class VerifyCsrfToken extends Middleware
{
    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array<int, string>
     */
    protected $except = [
        'api/*',
        '/login/submit',
        '/register',
        '/logout/submit',
        '/customers',
        '/edit/student/{id}',
        '/update/student/{id}',
        '/delete/student/{id}',
        '/sent/email',
        '/code/verify',
        '/change',
        '/edit/profile/{id}',
        '/profile',
    ];
}
