<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Forget Password Page</title>

    
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />

    <style>
        input[type=password],
select {
    width: 100%;
    padding: 12px 20px;
    margin: 8px 0px;
    display: inline-block;
    border: 1px solid #ccc;
    border-radius: 4px;
    box-sizing: border-box;
    margin-bottom: 20px;
}

input[type=submit] {
    width: 30%;
    background-color: #4CAF50;
    color: white;
    padding: 14px 20px;
    margin: 8px 0px 30px 570px;
    border: none;
    border-radius: 4px;
    cursor: pointer;
}

input[type=submit]:hover {
    background-color: #45a049;
}

div#change_password {
    border-radius: 5px;
    background-color: #f2f2f2;
    padding: 20px;
    margin-top: 55px;
}

a.fcc-btn {
    background-color: #4CAF50;
    color: white;
    padding: 15px 25px;
    text-decoration: none;
    margin: 8px 0px 0px 7px;
    border: none;
    border-radius: 4px;
    cursor: pointer;
    margin-top: 30px;
}

td#btn {
    padding: 20px;
}

.fcc-btn {
    background-color: #4CAF50;
    color: white;
    padding: 15px 25px;
    text-decoration: none;
    margin: 8px 0px 0px 7px;
    border: none;
    border-radius: 4px;
    cursor: pointer;
    margin-top: 30px;
}

.fcc-btn:hover {
    background-color: #45a049;
}

.formerror {
    color: red;
}

.succ {
    color: green;
    margin-left: 665px;
}
    </style>

</head>
<body>
<div>
                <h1 style="font-style: italic;"  >Verify Your Account</h1>
            </div>
            <hr>

	<div id="change_password">
		<form action="{{route('change')}}" id="form" method="POST" name="myForm">
        {{csrf_field()}}
			<div id="password">
			@if(session()->has('success'))
                    <div class="alert alert-success">
                    {{ session()->get('success') }}
                    </div>
                    @endif
			<br>
				<label>New Password : </label>
					<input type="password" id="pass" name="password" placeholder="New Password" class="form-control"  >
					@error('password')
                        <span class="text-danger">{{$message}}</span>
                    @enderror
				<br>
			</div>
			<div id="confirmpassword">
				<label>Re-type New : </label>
					<input type="password" id="cpass" name="confirmpassword" placeholder="Confirm Password" class="form-control" >
					@error('confirmpassword')
                        <span class="text-danger">{{$message}}</span>
                    @enderror
				<br>
			</div>
			<input type="submit" value="Save Change"></input>
		</form>
		<a class="fcc-btn" href="{{route('login')}}">Go to home</a>
	</div>
</body>
</html>