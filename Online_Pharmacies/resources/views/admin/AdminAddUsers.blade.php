<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Add Users</title>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.8.2/font/bootstrap-icons.css" integrity="sha384-eoTu3+HydHRBIjnCVwsFyCpUDZHZSFKEJD0mc3ZqSBSb6YhZzRHeiomAUWCstIWo" crossorigin="anonymous">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>            
    <script src="https://markcell.github.io/jquery-tabledit/assets/js/tabledit.min.js"></script>

    <style>
        *, ::before, ::after {
            box-sizing: border-box;
            font-size: small;
        }

        
@media (min-width: 992px){
        .col-md-1, .col-md-10, .col-md-11, .col-md-12, .col-md-2, .col-md-3, .col-md-4, .col-md-5, .col-md-6, .col-md-7, .col-md-8, .col-md-9 {
            float: initial;
        }
}
    </style>

    <style>
        input[type=text],
select {
    width: 100%;
    padding: 12px 20px;
    margin: 8px 0;
    display: inline-block;
    border: 1px solid #ccc;
    border-radius: 4px;
    box-sizing: border-box;
}

input[type=submit] {
    width: 100%;
    background-color: #4CAF50;
    color: white;
    padding: 14px 20px;
    margin: 8px 0;
    border: none;
    border-radius: 4px;
    cursor: pointer;
}

input[type=submit]:hover {
    background-color: #45a049;
}

input[type=password],
select {
    width: 100%;
    padding: 12px 20px;
    margin: 8px 0;
    display: inline-block;
    border: 1px solid #ccc;
    border-radius: 4px;
    box-sizing: border-box;
}

input[type=email],
select {
    width: 100%;
    padding: 12px 20px;
    margin: 8px 0;
    display: inline-block;
    border: 1px solid #ccc;
    border-radius: 4px;
    box-sizing: border-box;
}

input[type=number],
select {
    width: 100%;
    padding: 12px 20px;
    margin: 8px 0;
    display: inline-block;
    border: 1px solid #ccc;
    border-radius: 4px;
    box-sizing: border-box;
}

/* input[type=file],
select {
    width: 100%;
    padding: 12px 20px;
    margin: 8px 0;
    display: inline-block;
    border: 1px solid #ccc;
    border-radius: 4px;
    box-sizing: border-box;
} */

div#container1 {
    border-radius: 5px;
    background-color: #f2f2f2;
    padding: 20px;
}

div#container2 {
    border-radius: 5px;
    background-color: #f2f2f2;
    padding: 20px;
}

div#container3 {
    border-radius: 5px;
    background-color: #f2f2f2;
    padding: 20px;
}

.formerror {
    color: red;
}

.succ {
    color: green;
    margin-left: 665px;
}
    </style>

</head>
<body>
    @extends('layouts.app')
        @section('content')
            <div>
                <h1 style="font-style: italic;"  >ADD USERS</h1>
            </div>
            <hr>
        <br>
        @if(session()->has('success'))
            <div class="alert alert-success">
            {{ session()->get('success') }}
            </div>
        @endif
        <b><span class="succ" id="succ"></span></b>
        <br>
    <div id="container">
		<form action="" method="POST" id="form">
             {{csrf_field()}}
		<div id="container1">
				<br>
				<div class="col-75">
				<label>Username :</label>
				<input type="text" id="uname" name="uname" placeholder="User Name" value="{{old('uname')}}" class="form-control">
				@error('uname')
                        <span class="text-danger">{{$message}}</span>
                @enderror
				<br>
				</div>
				<div class="col-75">
				<label>Full Name :</label> <input type="text" id="fname" name="fname" placeholder="First Name" value="{{old('fname')}}" class="form-control">
				@error('fname')
                        <span class="text-danger">{{$message}}</span>
                @enderror
				<br>
				</div>
				<div class="col-75">
				<label>Address :</label> <input type="text" id="address" name="address" placeholder="Enter Address" value="{{old('address')}}" class="form-control">
				@error('address')
                        <span class="text-danger">{{$message}}</span>
                @enderror
		</div>
	</div>
	<br>
	<div id="container2">
                <label for="typeofngo">Gender: </label>
                <select class="gender" name="gender" class="form-control">

                <option>Select One</option>
                <option>Male</option>
                <option>Female</option>
                <option>Other</option>

                </select>
                @error('gender')
                        <span class="text-danger">{{$message}}</span>
                @enderror
					<br>
				<label>Password :</label> <input type="password" id="password" name="password" placeholder="Enter Password" value="{{old('password')}}" class="form-control">
                    @error('password')
                        <span class="text-danger">{{$message}}</span>
                    @enderror
						<br>
				<label>Confirm Password :</label>
                <input type="password" name="cpassword" value="{{old('cpassword')}}" placeholder="Enter Password" class="form-control">
                    @error('cpassword')
                        <span class="text-danger">{{$message}}</span>
                    @enderror
						<br>
	</div>
	<br>
	<div id="container3">
				<label>Phone Number :</label>
                <input type="number" name="Phone" value="{{old('Phone')}}" placeholder="Enter Phone Number" class="form-control">
                    @error('Phone')
                        <span class="text-danger">{{$message}}</span>
                    @enderror
                    <br>
                    <label>Upload Image :</label>
                    <input type="file" name="image" class="form-control">
                    @error('image')
                        <span class="text-danger">{{$message}}</span>
                    @enderror
                    <br>
                    <br>
				<input type="submit" name="Start" value="Submit">
	</div>
		</form>
	</div>
    @endsection
</body>
</html>

<script>

$(document).ready(function() {
    $('#form').on('submit', function(event) {
        event.preventDefault();
        var form_data = $(this).serialize();
        $.ajax({
            url: '{{ route("addusers") }}',
            method: "POST",
            data: form_data,
            dataType: "JSON",
        })
        alert("Added Successful");
        $("#form")[0].reset();
    });
});

</script>