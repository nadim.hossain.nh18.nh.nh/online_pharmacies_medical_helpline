<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Customers Management</title>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.8.2/font/bootstrap-icons.css" integrity="sha384-eoTu3+HydHRBIjnCVwsFyCpUDZHZSFKEJD0mc3ZqSBSb6YhZzRHeiomAUWCstIWo" crossorigin="anonymous">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>            
    <script src="https://markcell.github.io/jquery-tabledit/assets/js/tabledit.min.js"></script>


    <style>
        @media (min-width: 992px){
            .container {
                width: 1250px;
                font-size: medium;
                margin-left: -8px;
            }
        }

        *, ::before, ::after {
            box-sizing: border-box;
            font-size: small;
        }
        div#export{
          margin-left: 850px;
          margin-bottom: -34px;
        }
    </style>

</head>
<body>
    @extends('layouts.app')
        @section('content')
                <div>
                    <h1 style="font-style: italic;"  >CUSTOMERS LIST</h1>
                </div>
                <hr>
                <div class="container">
        <br />
        <h3 align="center">Users Controller</h3>
        </br>
        <div class="container">
        <input class="form-control mb-4" id="tableSearch" type="text"
            placeholder="Type something to search list items">
        <br>
            <div id="export">
                <a href="{{route('buyerexport')}}">
                <center><button type="button" class="btn btn-success" > <span class="glyphicon glyphicon-download-alt"></span> Export Data</button></center>
                </a>
            </div>
            <div style="margin-left: 1100px;">
                <a href="{{route('addusers')}}">
                <center><button type="button" class="btn btn-success" > <span class="glyphicon glyphicon-plus"></span> Add User</button></center>
                </a>
            </div>
        <br>

        <div class="panel panel-default">
            <div class="panel-heading">
              <h3 class="panel-title"> <span class="	glyphicon glyphicon-user">  </span> Cusromers Data</h3>
            </div>
            <div class="panel-body">
            <div class="table-responsive">
                @csrf
                <table id="editable" class="table table-bordered table-striped">
                <thead>
                    <tr>
                      <th>ID</th>
                      <th>User Name</th>
                      <th>Name</th>
                      <th>Address</th>
                      <th>Gender</th>
                      <th>Phone Number</th>
                      <th>Status</th>
                      <th>Image</th>
                    </tr>
                </thead>
                <tbody id="myTable">
                  @foreach($data as $row)
                    <tr>
                      <td>{{ $row->buyerid }}</td>
                      <td>{{ $row->username }}</td>
                      <td>{{ $row->name }}</td>
                      <td>{{ $row->address }}</td>
                      <td>{{ $row->gender }}</td>
                      <td>{{ $row->number }}</td>
                      <td>{{ $row->status }}</td>
                      <td>{{ $row->image }}</td>
                    </tr>
                  @endforeach
                </tbody>
                </table>
            </div>
            </div>
        </div>
        </div>
        <br>
        <hr>

        <div class="container">
        <br />
        <h3 align="center">Seller Controller</h3>
        </br>
        <div class="container">
        <input class="form-control mb-4" id="tableSearch2" type="text"
            placeholder="Type something to search list items">
        <br>
            <div id="export">
                <a href="{{route('sellerexport')}}">
                <center><button type="button" class="btn btn-success" > <span class="glyphicon glyphicon-download-alt"></span> Export Data</button></center>
                </a>
            </div>
            <div style="margin-left: 1100px;">
                <a href="{{route('addseller')}}">
                <center><button type="button" class="btn btn-success" > <span class="glyphicon glyphicon-plus"></span> Add Seller</button></center>
                </a>
            </div>
        <br>

        <div class="panel panel-default">
            <div class="panel-heading">
            <h3 class="panel-title"> <span class="	glyphicon glyphicon-user">  </span> Sellers Data</h3>
            </div>
            <div class="panel-body">
            <div class="table-responsive">
                @csrf
                <table id="editable2" class="table table-bordered table-striped">
                <thead>
                    <tr>
                      <th>ID</th>
                      <th>User Name</th>
                      <th>Name</th>
                      <th>Address</th>
                      <th>Phone Number</th>
                      <th>Status</th>
                      <th>Image</th>
                    </tr>
                </thead>
                <tbody id="myTable">
                  @foreach($seller as $sellers)
                    <tr>
                      <td>{{ $sellers->sellerid }}</td>
                      <td>{{ $sellers->username }}</td>
                      <td>{{ $sellers->name }}</td>
                      <td>{{ $sellers->address }}</td>
                      <td>{{ $sellers->number }}</td>
                      <td>{{ $sellers->status }}</td>
                      <td>{{ $sellers->image }}</td>
                    </tr>
                  @endforeach
                </tbody>
                </table>
            </div>
            </div>
        </div>
        </div>
    @endsection
</body>
</html>

<script type="text/javascript">
$(document).ready(function(){
   
  $.ajaxSetup({
    headers:{
      'X-CSRF-Token' : $("input[name=_token]").val()
    }
  });

  $('#editable').Tabledit({
    url:'{{ route("customers.action") }}',
    dataType:"json",
    columns:{
      identifier:[0, 'buyerid'],
      editable:[[1, 'username'], [2, 'name'], [3, 'address'], [4, 'gender'], [5, 'number'], [6, 'status']]
    },
    restoreButton:false,
    onSuccess:function(data, textStatus, jqXHR)
    {
      if(data.action == 'delete')
      {
        $('#'+data.id).remove();
      }
    }
  });

});

//seller

$(document).ready(function(){
   
   $.ajaxSetup({
     headers:{
       'X-CSRF-Token' : $("input[name=_token]").val()
     }
   });
 
   $('#editable2').Tabledit({
     url:'{{ route("seller.action") }}',
     dataType:"json",
     columns:{
       identifier:[0, 'sellerid'],
       editable:[[1, 'username'], [2, 'name'], [3, 'address'],  [4, 'number'], [5, 'status']]
     },
     restoreButton:false,
     onSuccess:function(data, textStatus, jqXHR)
     {
       if(data.action == 'delete')
       {
         $('#'+data.id).remove();
       }
     }
   });
 
 });

// Filter table

$(document).ready(function(){
  $("#tableSearch").on("keyup", function() {
    var value = $(this).val().toLowerCase();
    $("#myTable tr").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
  });
});


//seller search

$(document).ready(function(){
  $("#tableSearch2").on("keyup", function() {
    var value = $(this).val().toLowerCase();
    $("#myTable tr").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
  });
});
</script>