<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Dashboard</title>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.8.2/font/bootstrap-icons.css" integrity="sha384-eoTu3+HydHRBIjnCVwsFyCpUDZHZSFKEJD0mc3ZqSBSb6YhZzRHeiomAUWCstIWo" crossorigin="anonymous">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>            
    <script src="https://markcell.github.io/jquery-tabledit/assets/js/tabledit.min.js"></script>

    
    <script src="https://code.highcharts.com/highcharts.js"></script>
    <script src="https://code.highcharts.com/modules/exporting.js"></script>
    <script src="https://code.highcharts.com/modules/export-data.js"></script>
    <script src="https://code.highcharts.com/modules/accessibility.js"></script>

    <style>
        *, ::before, ::after {
            box-sizing: border-box;
            font-size: small;
        }

        div#box {
        margin-top: -4px;
        border-radius: 0px;
        background-color: white;
        padding: 53px;
        margin-left: -111px;
        margin-right: 0px;
        }
    
        div#pending {
            width: 304px;
            padding: 16px;
            margin-left: 54px;
            text-align: justify;
            border-radius: 8px;
            background-color: #337AB7;
            color: white;
            font-family: inherit;
            margin-bottom: 22px;
            margin-top: -38px;
        }
    
        div#accepted {
            width: 304px;
            padding: 16px;
            border-color: 3px solid green;
            margin-left: 381px;
            margin-top: -171px;
            border-radius: 8px;
            background-color: #5CB85C;
            color: white;
            font-family: inherit;
        }
    
        div#completed {
            width: 304px;
            padding: 16px;
            /* border: 3px solid green; */
            margin-left: 708px;
            margin-top: -149px;
            border-radius: 8px;
            background-color: #F0AD4E;
            color: white;
            font-family: inherit;
        }
    
        div#rejected {
            width: 304px;
            padding: 16px;
            /* border: 3px solid green; */
            margin-left: 1036px;
            margin-top: -147px;
            border-radius: 8px;
            background-color: #D9534F;
            color: white;
            font-family: inherit;
        }
        div#total {
            margin: 62px 0px 0px 0px;
            text-align: right;
            background-color: #337AB7;
        }
        div#total2 {
            margin: 62px 0px 0px 0px;
            text-align: right;
            background-color: #5CB85C;
        }
        div#total3 {
            margin: 62px 0px 0px 0px;
            text-align: right;
            background-color: #F0AD4E;
        }
        div#total4 {
            margin: 62px 0px 0px 0px;
            text-align: right;
            background-color: #D9534F;
        }
        div#UserTable{
            margin-right: 600px;
        }
        div#UserTable2{
            margin-left: 720px;
            margin-top: -378px;
            position:absolute;
        }
        div#UserTable3{
            margin-right: 600px;
            margin-top: -px;
        }
        div#UserTable4{
            margin-left: 720px;
            margin-top: -304px;
        }
        div#table1{
            /* / */
        }
        div#sellers{
            margin-top: 30px;
        }

        .table-bordered>tbody>tr>td, .table-bordered>tbody>tr>th, .table-bordered>tfoot>tr>td, .table-bordered>tfoot>tr>th, .table-bordered>thead>tr>td, .table-bordered>thead>tr>th {
            border: 1px solid #ddd;
            font-size: 14px;
        }


    </style>

</head>
<body>
    @extends('layouts.app')
        @section('content')
            <div>
                <h1 style="font-style: italic;"  >DASHBOARD</h1>
            </div>
            <hr>
            <div id="box">
                <div id="boder">
                    <div id="pending">
                        <div id="total">
                            <div id="countusers">
                            <b><h3>
                            {{$equaty}}
                            </h3></b>
                            </div>
                            <b>Total Earnings TK</b><br>
                        </div>
                    </div>
                </div>
                <div id="accepted">
                    <div id="total2">
                        <div id="countusers">
                        <b><h3>{{$buyer}} & {{$seller}}</h3></b>
                        </div>
                        <b>Total Users & Sellers</b><br>
                    <b></b>
                    </div>
                </div>
                <div id="completed">
                        <div id="total3">
                        <div id="countusers">
                            <b><h3>{{$activebuyer}} & {{$activeseller}}</h3></b>
                        </div>
                        <b>Total Active Users & Sellers</b><br>
                    <b></b>
                    </div>
                </div>
                <div id="rejected">
                    <div id="total4">
                        <div id="countusers">
                            <b><h3>{{$delivery}}</h3></b>
                        </div>
                        <b>Total Delivery Man</b><br>
                    <b></b>
                    </div>
                </div >
	        </div>
            <hr>
            <!-- <br>
            <div id="container"></div>
            <br>
            <hr> -->
            <div id="table1">
                <div id="UserTable">

                <div class="panel panel-default">
                    <div class="panel-heading">
                    <h3 class="panel-title"> <span class="glyphicon glyphicon-user">  </span> Customers</h3>
                    </div>
                    <div class="panel-body">
                <div class="table-responsive">
                        @csrf
                    <table id="editable" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                            <th>ID</th>
                            <th>Cusromers UserName</th>
                            <th>Status</th>
                            </tr>
                        </thead>
                        <tbody id="myTable">
                        @foreach($data as $row)
                            <tr>
                            <td>{{ $row->id }}</td>
                            <td>{{ $row->username }}</td>
                            <td>{{ $row->status }}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                    <nav aria-label="Page navigation example" style="margin-left: 550px;">
                        <ul class="pagination justify-content-end">
                        {!! $data->links() !!}
                        </ul>
                        </nav>
                    </div>
                </div>
                </div>
                </div>

                <div id="UserTable2">

                    <div class="panel panel-default">
                        <div class="panel-heading">
                        <h3 class="panel-title"> <span class="glyphicon glyphicon-bell">  </span> Recent Register</h3>
                        </div>
                        <div class="panel-body">
                        <div class="table-responsive">
                            @csrf
                            <table id="editable" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>User Name</th>
                                    <th>Date</th>
                                </tr>
                            </thead>
                            <tbody id="myTable">
                            @foreach($data as $row)

                                @php($diffInDays = \Carbon\Carbon::parse($row->created_at)->diffInDays())

                                @php($showDiff = \Carbon\Carbon::parse($row->created_at)->diffForHumans())

                                @if($diffInDays > 0)

                                    @php($showDiff .= ', '.\Carbon\Carbon::parse($row->created_at)->addDays($diffInDays)->diffInHours().' Hours')

                                @endif

                                <tr>
                                    <td>{{ $row->id }}</td>
                                    <td>{{ $row->username }}</td>
                                    <td>{{ $showDiff }}</td>
                                </tr>
                                @endforeach
                            </tbody>
                            </table>
                        </div>
                        <nav aria-label="Page navigation example" style="margin-left: 530px;">
                            <ul class="pagination justify-content-end">
                            {!! $data->links() !!}
                            </ul>
                            </nav>
                        </div>
                    </div>
                    </div>
                    </div>
                </div>
                <hr>
            <div id="sellers" >
                <div id="UserTable3">

                <div class="panel panel-default">
                    <div class="panel-heading">
                    <h3 class="panel-title"> <span class="	glyphicon glyphicon-user">  </span>Sellers & Channels</h3>
                    </div>
                    <div class="panel-body">
                    <div class="table-responsive">
                        @csrf
                        <table id="editable" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                            <th>ID</th>
                            <th>Sellers Name</th>
                            <th>Channel Name</th>
                            </tr>
                        </thead>
                        <tbody id="myTable">
                                @foreach($cname as $name)
                                    <tr>
                                        <td>{{ $name->sellerid }}</td>
                                        <td>{{ $name->username }}</td>
                                        <td>{{ $name->channelname }}</td>
                                    </tr>
                                @endforeach
                        </tbody>
                        </table>
                    </div>
                    <nav aria-label="Page navigation example" style="margin-left: 1070px;">
                        <ul class="pagination justify-content-end">
                        {!! $data->links() !!}
                        </ul>
                        </nav>
                    </div>
                </div>
                </div>
                </div>
                <div id="UserTable4">

                <div class="panel panel-default">
                    <div class="panel-heading">
                    <h3 class="panel-title"> <span class="glyphicon glyphicon-stats">  </span>Channels & Products</h3>
                    </div>
                    <div class="panel-body">
                    <div class="table-responsive">
                        @csrf
                        <table id="editable" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                            <th>ID</th>
                            <th>Channels Name</th>
                            <th>Total Products</th>
                            </tr>
                        </thead>
                        <tbody id="myTable">
                        @foreach($pname as $name)
                                    <tr>
                                        <td>{{ $name->channelid }}</td>
                                        <td>{{ $name->channelname }}</td>
                                        <td>{{ $name->product_quantity }}</td>
                                    </tr>
                                @endforeach
                        </tbody>
                        </table>
                    </div>
                    <nav aria-label="Page navigation example" style="margin-left: 1070px;">
                        <ul class="pagination justify-content-end">
                        
                        </ul>
                        </nav>
                    </div>
                </div>
                </div>
                </div>
            </div>
    @endsection
</body>
</html>

<script src="https://code.highcharts.com/highcharts.js"></script>

<script type="text/javascript">
    var userData = <?php echo json_encode($userData)?>;
    Highcharts.chart('container', {
        title: {
            text: 'New User 2021'
        },
        subtitle: {
            text: 'Bluebird youtube channel'
        },
        xAxis: {
            categories: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September',
                'October', 'November', 'December'
            ]
        },
        yAxis: {
            title: {
                text: 'Number of New Users'
            }
        },
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'middle'
        },
        plotOptions: {
            series: {
                allowPointSelect: true
            }
        },
        series: [{
            name: 'New Users',
            data: userData
        }],
        responsive: {
            rules: [{
                condition: {
                    maxWidth: 500
                },
                chartOptions: {
                    legend: {
                        layout: 'horizontal',
                        align: 'center',
                        verticalAlign: 'bottom'
                    }
                }
            }]
        }
    });
</script>