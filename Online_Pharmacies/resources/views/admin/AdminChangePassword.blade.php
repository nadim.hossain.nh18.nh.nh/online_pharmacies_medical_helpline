<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Change Password</title>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.8.2/font/bootstrap-icons.css" integrity="sha384-eoTu3+HydHRBIjnCVwsFyCpUDZHZSFKEJD0mc3ZqSBSb6YhZzRHeiomAUWCstIWo" crossorigin="anonymous">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>            
    <script src="https://markcell.github.io/jquery-tabledit/assets/js/tabledit.min.js"></script>


    <style>
        *, ::before, ::after {
            box-sizing: border-box;
            font-size: small;
        }

            
    input[type=password],
    select {
        width: 100%;
        padding: 12px 20px;
        margin: 8px 0;
        display: inline-block;
        border: 1px solid #ccc;
        border-radius: 4px;
        box-sizing: border-box;
    }
    
    input[type=submit] {
        width: 100%;
        background-color: #4CAF50;
        color: white;
        padding: 14px 20px;
        margin: 8px 0;
        border: none;
        border-radius: 4px;
        cursor: pointer;
    }
    
    input[type=submit]:hover {
        background-color: #45a049;
    }
    
    div#change_password {
        border-radius: 5px;
        background-color: #f2f2f2;
        padding: 20px;
        margin-top: 65px;
    }
    
    div#total {
        margin-top: -10px;
    }
    
    div#profile {
        border-radius: 5px;
        background-color: #f2f2f2;
        padding: 20px;
        margin-top: 30px;
    }
    
    /* form label {
        font-size: 18px;
        padding: 7px;
    } */
    .height-100 {
    height: 93vh;
    }
    </style>
</head>
<body>
    @extends('layouts.app')
        @section('content')
            <div>
                <h1 style="font-style: italic;"  >CHANGE PASSWORD</h1>
                <hr>
                <div id="change_password">
                    @if(session()->has('error'))
                        <div class="alert alert-danger">
                        {{ session()->get('error') }}
                        </div>
                    @endif
                    @if(session()->has('success'))
                    <div class="alert alert-success">
                    {{ session()->get('success') }}
                    </div>
                    @endif
                    <form action="{{route('changepassword')}}" method="POST" id="form">
                    {{csrf_field()}}
                            <label>Old Password :</label> <input type="password" id="oldpassword" name="oldpassword" value="{{old('oldpassword')}}" placeholder="Old Password" class="form-control">
                            <b>@error('oldpassword')
                                        <span class="text-danger">{{$message}}</span>
                                @enderror</b>
                            <br>
                            <label> New Password : </label> <input type="password" id="newpassword" name="newpassword" value="{{old('newpassword')}}" placeholder="New Password" class="form-control">
                            <b>@error('newpassword')
                                    <span class="text-danger">{{$message}}</span>
                            @enderror</span></b>
                            <br>
                            <label> Re-type New : </label> <input type="password" id="retypenew" name="retypenew" value="{{old('retypenew')}}" placeholder="Confirm Password" class="form-control">
                            <b>@error('retypenew')
                                    <span class="text-danger">{{$message}}</span>
                            @enderror</b>
                            <br>
                        <input type="submit" value="Save Change"></input>
                    </form>
                </div>
           </div>
    @endsection
</body>
</html>

<!-- <script>
    $('#form').on('submit', function(e){
        e.preventDefault();

        $.ajax({
            url:$(this).attr('action'),
            metthod:$(this).attr('method'),
            data:new FormData(this),
            processData:false,
            dataType:'json',
            contentType:false,
            beforeSent:function(){
                $(document).find('span.error.text').text('');
            },
            success:function(data){
                if(data.status == 0){
                    $.each(data.error, function(prefix, val){
                        $('span.'+prefix+'_error').text(val[0]);
                    });
                }
                else{
                    $('#form')[0].reset();
                    alert(data.msg);
                }
            }
        });

    });

</script> -->