<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Verify User</title>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />

    <style>
        body {
    background-color: white;
}

div#verify {
    border-radius: 5px;
    background-color: #f2f2f2;
    padding: 20px;
    margin-top: 55px;
    align-content: center;
}

input[type=submit] {
    width: 100%;
    background-color: #4CAF50;
    color: white;
    padding: 14px 20px;
    margin: 8px 0;
    border: none;
    border-radius: 4px;
    cursor: pointer;
    margin-left: 45%;
}

input[type=submit]:hover {
    background-color: #45a049;
}

input[type=email],
select {
    width: 100%;
    padding: 12px 20px;
    margin: 8px 0;
    display: inline-block;
    border: 1px solid #ccc;
    border-radius: 4px;
    box-sizing: border-box;
    align-content: center;
}

input[type=number],
select {
    width: 100%;
    padding: 12px 20px;
    margin: 8px 0;
    display: inline-block;
    border: 1px solid #ccc;
    border-radius: 4px;
    box-sizing: border-box;
    align-content: center;
}

form h3 {
    text-align: center;
}

form label {
    text-align: center;
}

.formerror {
    color: red;
}
    </style>

</head>
<body>
            <div>
                <h1 style="font-style: italic;"  >Verify Your Account</h1>
            </div>
            <hr>
	<div id="verify">
                @if(session()->has('success'))
                    <div class="alert alert-success">
                    {{ session()->get('success') }}
                    </div>
                    @endif
                    @if(session()->has('error'))
                    <div class="alert alert-danger">
                    {{ session()->get('error') }}
                    </div>
                    @endif
        <form action="{{route('email')}}" method="POST">

        {{csrf_field()}}

			<h3>Enter your Email address To Verity Your Account.</h3>
			<div id="email">
				<label>Email Addresss :</label>
					<input type="email" name="email" id="email" placeholder="Enter Email Address" class="form-control" >
					<br>
                    @error('email')
                        <span class="text-danger">{{$message}}</span>
                    @enderror
			</div>

			<input type="submit" name="Verify" value="Sent" style="width: 15%;">

        </form>
        <div id="code">
            <form action="{{route('code.verify')}}" method="POST" >
                {{csrf_field()}}
                    <div id="code">
                            <label>Code :</label>
                            <input type="number" name="code" id="code" placeholder="Enter Corrent Code" class="form-control" >
                            <br>
                            @error('code')
                                    <span class="text-danger">{{$message}}</span>
                            @enderror
                    </div>
                    <input type="submit" name="Verify" value="Verify" style="width: 15%;">
            </form>
	</div>
    <a href="{{route('login')}}">
	    <input type="submit" name="Go Back" value="Go Back" style="width: 15%;">
    </a>
</body>
</script>
</html>