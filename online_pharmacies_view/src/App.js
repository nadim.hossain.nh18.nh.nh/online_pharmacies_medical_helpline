import React from 'react';
import ReactDOM from 'react-dom/client';
import 'bootstrap/dist/css/bootstrap.min.css';
import './index.css';
import Dashboard from './Components/Dashboard';
import Users from './Components/Users';
import {Navigate} from 'react-router-dom';
import Payments_Getaway from './Components/Payments_Getaway';
import Inventory from './Components/Inventory';
import Discount_Coupons from './Components/Discount_Coupons';
import Accounts from './Components/Accounts';
import Payment from './Components/Payment';
import Profile from './Components/Profile';
import Change_Password from './Components/Change_Password';
import Sign_Out from './Components/Sign_Out';
import Login from './Components/Login/Login';
import Header from './Components/Header.js';
import AddUser from './Components/AddUser';
import reportWebVitals from './reportWebVitals';
import {BrowserRouter as Router, Route, Routes} from 'react-router-dom';
import axios from 'axios';
axios.defaults.withCredentials = true;

axios.defaults.baseURL = "http://127.0.0.1:8000/";
axios.defaults.headers.post['Content-Type'] = 'multipart/form-data; boundary=<calculated when request is sent>';
axios.defaults.headers.post['Accept'] = 'application/json';
axios.interceptors.request.use(function (config){
  const token = localStorage.getItem('auth_token');
  config.headers.Authorization = token ? `Bearer ${token}` : '';
  return config;
});

function App() {
  return (
    <div className="App">
      <Header/>
      <Router>
      <Header/>
      <Routes>
      {/* <Route exact path='/' element={<Dashboard/>} />
      <Route exact path='/dashboard' element={<Dashboard/>} />
        <Route exact path='/register' element={<Users/>} />
        <Route exact path='/payments' element={<Payments_Getaway/>} />
        <Route exact path='/inventory' element={<Inventory/>} />
        <Route exact path='/discount' element={<Discount_Coupons/>} />
        <Route exact path='/accounts' element={<Accounts/>} />
        <Route exact path='/payment/clint' element={<Payment/>} />
        <Route exact path='/profile' element={<Profile/>} />
        <Route exact path='/login' element={<Login/>} />
        <Route exact path='/adduser' element={<AddUser/>} />
        <Route exact path='/logout' element={<Sign_Out/>} /> */}
        <Route path='/login'>
          {localStorage.getItem('auth_token') ? <Navigate to="/dashboard" />: <Login></Login>}
        </Route>
      </Routes>
    </Router>
    </div>
  );
}

export default App;
