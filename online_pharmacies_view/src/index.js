import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import {BrowserRouter as Router, Route, Routes, Redirect} from 'react-router-dom';
import {Navigate} from 'react-router-dom';
import App from './App';
import Header from "./Components/Header";
import reportWebVitals from './reportWebVitals';
import 'bootstrap/dist/css/bootstrap.min.css';
import Dashboard from './Components/Dashboard';
import Users from './Components/Users';
import Payments_Getaway from './Components/Payments_Getaway';
import Inventory from './Components/Inventory';
import Discount_Coupons from './Components/Discount_Coupons';
import Accounts from './Components/Accounts';
import Payment from './Components/Payment';
import Profile from './Components/Profile';
import Change_Password from './Components/Change_Password';
import Sign_Out from './Components/Sign_Out';
import Login from './Components/Login/Login';
import AddUser from './Components/AddUser';
import EditUser from './Components/EditUser';
import Verify from './Components/Modals/Verify';
import Change from './Components/Modals/Change';
import EditUser2 from './Components/EditUser2';
import axios from 'axios';


axios.defaults.headers.common['X-CSRF-TOKEN'] = window.csrf_token;

axios.defaults.baseURL = "http://127.0.0.1:8000/";
axios.defaults.headers.post['Content-Type'] = 'application/json';
axios.defaults.headers.post['Accept'] = 'application/json';

axios.defaults.withCredentials = true;

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <Router>
      <Header/>
      <Routes>
      <Route exact path='/' element={<Dashboard/>} />
      <Route exact path='/dashboard' element={<Dashboard/>} />
        <Route exact path='/register' element={<Users/>} />
        <Route exact path='/payments' element={<Payments_Getaway/>} />
        <Route exact path='/inventory' element={<Inventory/>} />
        <Route exact path='/discount' element={<Discount_Coupons/>} />
        <Route exact path='/accounts' element={<Accounts/>} />
        <Route exact path='/payment/clint' element={<Payment/>} />
        <Route exact path="/profile" element={<Profile/>} />
        <Route exact path='/login' element={<Login/>} />
        <Route exact path='/adduser' element={<AddUser/>} />
        <Route exact path='/logout' element={<Sign_Out/>} />
        <Route exact path='/verify/user' element={<Verify/>} />
        <Route exact path='/change' element={<Change/>} />
        <Route exact path="/edit/student/:id" element={<EditUser2/>} />
        {/* <Route exact path='/login'>
          {localStorage.getItem('auth_token') ? <Navigate to="/dashboard" />: <Login />}
        </Route> */}
      </Routes>
    </Router>
  </React.StrictMode>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
