import axios from "axios";
import React, { Component } from "react";
import { useState, useEffect } from 'react';
import swal from "sweetalert";
import { useNavigate } from 'react-router-dom';
const Verify = () => {
    const navigate = useNavigate();

    // useEffect(() =>{
    //     if(localStorage.getItem('')){
    //         navigate('/dashboard')
    //     }
    // },[])
    // const [username, setUname] = useState("");
    // const [password, setPassword] = useState("");


    // async function loginsubmit(){
    //     let Item ={username,password}
    //     console.warn(Item);
    // }
    const [loginInput, setLogin] = useState({
        email: '',
        error_list: [],
    });
    const [loginInput1, setLogin1] = useState({
        code: '',
        error_list: [],
    });
    const heandleInput = (e)=>{
        e.persist();
        setLogin({...loginInput, [e.target.name]: e.target.value});
    }

    const heandleInput1 = (e)=>{
        e.persist();
        setLogin1({...loginInput1, [e.target.name]: e.target.value});
    }

    const loginSubmit = (e) =>{
        e.preventDefault();
        const data = {
            email: loginInput.email,
        }
        axios.get('/sanctum/csrf-cookie').then(response => {
            axios.post(`api/sent/email`, data).then(res =>{
                if(res.data.status === 200){
                    localStorage.setItem('code', res.data.code);
                    localStorage.setItem('email', res.data.email);
                    swal("Success", res.data.message, "success");
                    navigate('/verify/user');
                }
                else if(res.data.status === 401){
                    swal("Warning",res.data.message,"warning");
                    navigate('/verify/user');
                }
                else
                setLogin({...loginInput, error_list: res.data.errors});
            });
        });
    }
    const loginSubmit1 = (e) =>{
        e.preventDefault();
        const data = {
            code: loginInput1.code,
        }
        axios.get('/sanctum/csrf-cookie').then(response => {
            axios.post(`api/code/verify`, data).then(res =>{
                if(res.data.status === 200){
                    swal("Success", res.data.message, "success");
                    navigate('/change');
                }
                else if(res.data.status === 401){
                    swal("Warning",res.data.message,"warning");
                }
                else
                setLogin1({...loginInput1, error_list: res.data.errors});
            });
        });
        // let varify_code = localStorage.getItem('code');
        // if(varify_code === loginInput1.code){
        //     swal("Success", "Verify Successfull", "success");

        //     navigate('/change');
        // }
        // else{
        //     swal("Warning","Invalide Code","warning");
        //     navigate('/verify/user');
        // }
    }
    return (
        <div className="col-sm-6 offset-sm-3">
                <h1>Verify User PAGE</h1>
                <hr></hr>
                        <br/>
                <form onSubmit={loginSubmit}>
                            <label>Email :</label>
                            <input type="email" placeholder="Email Address" name="email" value={loginInput.email} onChange={heandleInput} className="form-control" />
                            <span>{loginInput.error_list.email}</span>
                            <br/>
                    <button className="btn btn-primary" >Sent</button>
                    <br></br>
                </form>
                <br></br>
                <form onSubmit={loginSubmit1}>
                            <label>Code :</label>
                            <input type="number" placeholder="Code" name="code" value={loginInput1.code} onChange={heandleInput1} className="form-control" />
                            <span>{loginInput1.error_list.code}</span>
                            <br/>
                    <button className="btn btn-primary" >verify</button>
                    <br></br>
                </form>
        </div>
    );
}

export default Verify;