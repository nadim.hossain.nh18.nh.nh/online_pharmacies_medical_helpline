import axios from "axios";
import React, { Component } from "react";
import { useState, useEffect } from 'react';
import swal from "sweetalert";
import { useNavigate } from 'react-router-dom';
import { Link } from "react-router-dom";
const Login = () => {
    const navigate = useNavigate();

    useEffect(() =>{
        if(localStorage.getItem('auth_token')){
            navigate('/dashboard')
        }
    },[])
    // const [username, setUname] = useState("");
    // const [password, setPassword] = useState("");


    // async function loginsubmit(){
    //     let Item ={username,password}
    //     console.warn(Item);
    // }
    const [loginInput, setLogin] = useState({
        uname: '',
        password:'',
        error_list: [],
    });
    const heandleInput = (e)=>{
        e.persist();
        setLogin({...loginInput, [e.target.name]: e.target.value});
    }

    const loginSubmit = (e) =>{
        e.preventDefault();
        const data = {
            uname: loginInput.uname,
            password: loginInput.password,
        }
        axios.get('/sanctum/csrf-cookie').then(response => {
            axios.post(`api/login/submit`, data).then(res =>{
                if(res.data.status === 200){
                    localStorage.setItem('auth_token', res.data.token);
                    localStorage.setItem('auth_name', res.data.username);
                    swal("Success", res.data.message, "success");
                    navigate('/dashboard');
                }
                else if(res.data.status === 401){
                    swal("Warning",res.data.message,"warning");
                }
                else
                setLogin({...loginInput, error_list: res.data.errors});
            });
        });
    }
    return (
        <div className="col-sm-6 offset-sm-3">
                <h1>LOGIN PAGE</h1>
                <hr></hr>
                        <br/>
                        <form onSubmit={loginSubmit}>
                            <label>Username :</label>
                            <input type="text" placeholder="User Name" name="uname" vname='uname' value={loginInput.uname} onChange={heandleInput} className="form-control" />
                            <span>{loginInput.error_list.uname}</span>
                            <br/>
                        <label>Password :</label> 
                        <input type="password"  placeholder="Enter Password" name="password" value={loginInput.password} onChange={heandleInput} className="form-control"/>
                        <span>{loginInput.error_list.password}</span>
                                <br/>
                    <button className="btn btn-primary" >Submit</button>
                    <br></br>
                </form>
                <br></br>
                <Link to="/verify/user">
                    Forget Password?
                </Link>
        </div>
    );
}

export default Login;