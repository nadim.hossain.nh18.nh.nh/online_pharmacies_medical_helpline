import React, { Component } from "react";
import { useState, useEffect } from 'react';
import { useNavigate } from 'react-router-dom';
const Payments_Getaway = () => {
    const navigate = useNavigate();

    useEffect(() =>{
        if(!localStorage.getItem('auth_token')){
            navigate('/login')
        }
    },[])
    return (
        <h1>Payments Getaway</h1>
    );
}

export default Payments_Getaway;  