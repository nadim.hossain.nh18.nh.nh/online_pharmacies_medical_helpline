import React, { Component } from "react";
// import '../ChangePass.css'
import { useState, useEffect } from 'react';
import { useNavigate } from 'react-router-dom';
const Change_Password = () => {

    const navigate = useNavigate();

    useEffect(() =>{
        if(!localStorage.getItem('auth_token')){
            navigate('/login')
        }
    },[])
    return (
        <div className="ChangePass">
                <h1>CHANGE PASSWORD</h1>
                <hr/>
                <div id="change_password">
                    <form action="{{route('changepassword')}}" method="POST" id="form">
                            <label>Old Password :</label> 
                            <input type="password" id="oldpassword" name="oldpassword" value="{{old('oldpassword')}}" placeholder="Old Password" className="form-control"/>
                            <br/>
                            <label> New Password : </label> 
                            <input type="password" id="newpassword" name="newpassword" value="{{old('newpassword')}}" placeholder="New Password" className="form-control"/>
                            <br/>
                            <label> Re-type New : </label> 
                            <input type="password" id="retypenew" name="retypenew" value="{{old('retypenew')}}" placeholder="Confirm Password" className="form-control"/>
                            <br/>
                        <input type="submit" value="Save Change"></input>
                    </form>
                </div>
        </div>
    );
}

export default Change_Password;  