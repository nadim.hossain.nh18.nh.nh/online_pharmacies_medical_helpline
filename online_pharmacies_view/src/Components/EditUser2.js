import {Link, useHistory} from 'react-router-dom';
import React, {useState, useEffect} from 'react';
import axios from 'axios';
import swal from 'sweetalert';
import { useParams } from "react-router-dom";
import { useNavigate } from 'react-router-dom';

const EditUser2 = () => {

    const params = useParams();

    const navigate = useNavigate();
    const [loading, setLoading] = useState(true);
    const [studentInput, setStudent] = useState([]);
    const [errorInput, setError] = useState([]);

    useEffect(() => {
        
        const id = params.id;
        axios.get('/sanctum/csrf-cookie').then(response => {
        axios.get(`api/edit/student/${id}`).then( res => {

            if(!localStorage.getItem('auth_token')){
                navigate('/login')
            }
            else{
                
                if(res.data.status === 200)
            {
                setStudent(res.data.student);
                setLoading(false);
            }
            else if(res.data.status === 404)
            {
                swal("Error",res.data.message,"error");
                navigate('/register');
            }
            }
        });
    });

    }, [navigate]);

    const handleInput = (e) => {
        e.persist();
        setStudent({...studentInput, [e.target.name]: e.target.value });
    }

    const updateStudent = (e) => {
        e.preventDefault();
        
        const id = params.id;
        const data = {
            name: studentInput.name,
            address: studentInput.address,
            gender: studentInput.gender,
            mobile: studentInput.mobile,
        }
        axios.get('/sanctum/csrf-cookie').then(response => {
        axios.put(`api/update/student/${id}`, data).then(res=>{
            if(res.data.status === 200)
            {
                swal("Success",res.data.message,"success");
                setError([]);
                navigate('/register');
            }
            else if(res.data.status === 422)
            {
                swal("All fields are mandetory","","error");
                setError(res.data.validationErrors);
            }
            else if(res.data.status === 404)
            {
                swal("Error",res.data.message,"error");
                navigate('/register');
            }
        });
    });
    }

    if(loading)
    {
        return <h4>Loading Edit Student Data...</h4>
    }
    
    return (
        <div>
            <h1>USER EDIT PAGE</h1>
            <hr />
            <div className="container">
                <div className="row justify-content-center">
                    <div className="col-md-6">
                        <div className="card">
                            <div className="card-header">
                                <h4>Edit user 
                                    <Link to={'/register'} className="btn btn-primary btn-sm float-end"> BACK</Link>
                                </h4>
                            </div>
                            <div className="card-body">
                                
                                <form onSubmit={updateStudent}>
                                    <div className="form-group mb-3">
                                        <label>Full Name</label>
                                        <input type="text" name="name" onChange={handleInput} value={studentInput.name}  className="form-control" />
                                        <span className="text-danger">{errorInput.name}</span>
                                    </div>
                                    <div className="form-group mb-3">
                                        <label>Address</label>
                                        <input type="text" name="address" onChange={handleInput} value={studentInput.address}  className="form-control" />
                                        <span className="text-danger">{errorInput.address}</span>
                                    </div>
                                    <div className="form-group mb-3">
                                        <label>Phone Number</label>
                                        <input type="text" name="mobile" onChange={handleInput} value={studentInput.mobile}  className="form-control" />
                                        <span className="text-danger">{errorInput.mobile}</span>
                                    </div>
                                    <div className="form-group mb-3">
                                    <label>Gender: </label>
                                        <select className="form-control" name='gender' value={studentInput.gender} onChange={handleInput} >

                                        <option>Select One</option>
                                        <option>Male</option>
                                        <option>Female</option>
                                        <option>Other</option>

                                        </select>
                                        <span className="text-danger">{errorInput.gender}</span>
                                    </div>
                                    <div className="form-group mb-3">
                                        <button type="submit" className="btn btn-primary">Update User</button>
                                    </div>
                                </form>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );

}

export default EditUser2;