import React, { Component } from "react";
import { useState, useEffect } from 'react';
import { useNavigate } from 'react-router-dom';
const Accounts = () => {
    const navigate = useNavigate();

    useEffect(() =>{
        if(!localStorage.getItem('auth_token')){
            navigate('/login')
        }
    },[])
    return (
        // <div className="Account">
        //     <div>
        //     <div>
        //         <h1>ACCOUNTS</h1>
        //     </div>
        //     <div id="box">
        //         <div id="boder">
        //             <div id="pending">
        //                 <div id="total">
        //                     <div id="countusers">
        //                         <b><h3>
        //                             {/* {{ $earm }} */}
        //                         </h3></b>
        //                     </div>
        //                     <b>Total Assets</b><br/>
        //                     </div>
        //             </div>
        //         </div>
        //         <div id="accepted">
        //             <div id="total2">
        //                 <div id="countusers">
        //                     <b><h3>
        //                         {/* {{ $liabilities }} */}
        //                     </h3></b>
        //                 </div>
        //                 <b>Total Liabilities</b><br/>
        //                     <b></b>
        //                 </div>
        //         </div>
        //         <div id="completed">
        //             <div id="total3">
        //                 <div id="countusers">
        //                     <b><h3>
        //                         {/* {{ $equaty }} */}
        //                     </h3></b>
        //                 </div>
        //                 <b>Total Equity</b><br/>
        //                     <b></b>
        //                 </div>
        //         </div>
        //         <div id="rejected">
        //             <div id="total4">
        //                 <div id="countusers">
        //                     <b><h3>
        //                         {/* {{ $tmoney }} */}
        //                     </h3></b>
        //                 </div>
        //                 <b>Today Total Added Money TK</b><br/>
        //                     <b></b>
        //                 </div>
        //         </div>
        //     </div>
        //         <div id="UserTable">

        //             <div class="panel panel-default">
        //                 <div class="panel-heading">
        //                     <h3 class="panel-title"> <span class="glyphicon glyphicon-usd">  </span> Payments Details</h3>
        //                 </div>
        //                 <div class="panel-body">
        //                     <div class="table-responsive">
        //                         <table id="editable" class="table table-bordered table-striped">
        //                             <thead>
        //                                 <tr>
        //                                     <th>ID</th>
        //                                     <th>Channels Name</th>
        //                                     <th>Amount</th>
        //                                     <th>Bank Account Number</th>
        //                                 </tr>
        //                             </thead>
        //                             <tbody id="myTable">
        //                                 {/* @foreach($pays as $pay)

        //                                 <tr>
        //                                     <td>{{ $pay } - > id}{"}"}</td>
        //                                     <td>{{ $pay } - > channelname}&rbrace;</td>
        //                                     <td>{{ $pay } - > amount}}</td>
        //                                     <td>{{ $pay } - > bankaccountnumber}&rbrace;</td>
        //                                 </tr>
        //                                 @endforeach */}
        //                             </tbody>
        //                         </table>
        //                     </div>
        //                     {/* <nav aria-label="Page navigation example" style="margin-left: px;">
        //                         <ul class="pagination justify-content-end">
        //                             {!!$pays - > links()!!}
        //                         </ul>
        //                     </nav> */}
        //                 </div>
        //             </div>
        //         </div>
        //     </div><div id="UserTable2">

        //         <div class="panel panel-default">
        //             <div class="panel-heading">
        //                 <h3 class="panel-title"> <span class="glyphicon glyphicon-bell">  </span> Recent Add Money</h3>
        //             </div>
        //             <div class="panel-body">
        //                 <div class="table-responsive">
        //                     <table id="editable" class="table table-bordered table-striped">
        //                         <thead>
        //                             <tr>
        //                                 <th>ID</th>
        //                                 <th>Money</th>
        //                                 <th>Date</th>
        //                             </tr>
        //                         </thead>
        //                         <tbody id="myTable">
        //                             {/* @foreach($moneys as $money)

        //                             @php($diffInDays = \Carbon\Carbon::parse($money-&gt;date)-&gt;diffInDays())

        //                             @php($showDiff = \Carbon\Carbon::parse($money->date)->diffForHumans())

        //                             @if($diffInDays > 0)

        //                             @php($showDiff .= ', '.\Carbon\Carbon::parse($money->date)->addDays($diffInDays)->diffInHours().' Hours')

        //                             @endif


        //                             <tr>
        //                                 <td>{{ $money } - > orderid}&rbrace;</td>
        //                                 <td>{{ $money } - > tprice}}</td>
        //                                 <td>{{ $showDiff }}</td>
        //                             </tr>
        //                             @endforeach */}
        //                         </tbody>
        //                     </table>
        //                 </div>
        //                 {/* <nav aria-label="Page navigation example" style="margin-left: px;">
        //                     <ul class="pagination justify-content-end">
        //                         {!!$moneys - > links()!!}
        //                     </ul>
        //                 </nav> */}
        //             </div>
        //         </div>
        //     </div>
        // </div>
        <div>
            <h1>Account</h1>
        </div>
    );
}

export default Accounts;  